//
// Created by paulo on 12/10/22.
//

#include "garray.test.h"

void
garray_test () {
  garray *arr = garray_new (int);

  garray_push (arr, 1);
  garray_push (arr, 42);
  garray_push (arr, 2);
  garray_push (arr, 3);
  garray_push (arr, 4);
  garray_push (arr, 5);

  assert (arr->length == 6);

  assert (garray_get (arr, int, 0) == 1);
  assert (garray_get (arr, int, 1) == 42);
  assert (garray_get (arr, int, 2) == 2);
  assert (garray_get (arr, int, 3) == 3);
  assert (garray_get (arr, int, 4) == 4);
  assert (garray_get (arr, int, 5) == 5);

  int f = garray_take (arr, int, 1);

  assert (f == 42);

  for (size_t i = 0; i < arr->length; i++) {
    ft_printf ("%lu: %d\n", i, garray_get (arr, int, i));
    assert (garray_get (arr, int, i) == (int) i + 1);
  }

  garray_set (arr, 3, 33);

  assert (garray_get (arr, int, 3) == 33);

  size_t old_length = arr->length;

  garray_remove(arr, 3);
  assert (arr->length < old_length);

  garray_push(arr, 365);
  int last = garray_pop (arr, int);
  assert (last == 365);

  garray_destroy (&arr);
}
