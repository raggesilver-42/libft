<div align="center">
    <h1>
        <img src="https://imgur.com/wDuy8yC.png" width="128" alt="libft logo" />
    </h1>
    <h2>Essential C library</h2>
    <p>
        <a href="#compile">Compile</a> •
        <a href="#use">Use</a> •
        <a href="https://gitlab.com/raggesilver-42/libft/blob/master/LICENSE">License</a> •
        <a href="#credits">Credits</a>
    </p>
    <p>
        <a href="https://gitlab.com/raggesilver-42/libft/pipelines">
            <img src="https://gitlab.com/raggesilver-42/libft/badges/master/pipeline.svg" alt="Build Status" />
        </a>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="My profile on Patreon" />
        </a>
    </p>
</div>

This is libft, my "std" library for C. It started as my initial project at
[42][42] and has since evolved into a requirement for my C projects.

Libft reimplements* printf, `string.h`, and `libc`.

> \* - libc and string.h are only partially reimplemented

## Compile

```bash
make -j # all, clean, flcean, re, ci, debug
```

## Use

Making produces
```
.
├── build/
│   ├── src/      # Conatins object files and make rules for every object
│   └── includes/ # Contains all public headers
└── libft.a
```

If you're at 42 you probably shouldn't use someone else's libft —
[especially mine](norm-compliance-disclaimer) — if you're not, this is how I add
libft to my projects:

```bash
# Add libft as a submodule to your project
git submodule add git@gitlab.com:raggesilver-42/libft.git libft
# Then add libft to your makefile rules
```

## Norm Compliance Disclaimer

I'm no longer a cadet at 42, but I still like to use my libft. To better suit
my needs **it is no longer compliant with the norm**. C is limited enough as
is for me to not use macros, switches, for-loops, or to only be able to write
25-line-long functions. [This][norm_hash] is the last norm-compliant commit in
this repository.

My original libft was graded 125% and served me very well for every C project
I worked on at 42.

## Credits

Functions based on other projects are properly attributed in their files.

[norm_hash]: https://gitlab.com/raggesilver-42/libft/-/commit/b6830281741ab685755d7c3032ad1dd97f864315
[42]: https://www.42.us.org/
