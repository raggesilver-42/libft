#include "libft.h"

void
ft_memswap (void *_ptr1, void *_ptr2, size_t size)
{
  switch (size) {
    case 1: {
      unsigned char tmp;
      unsigned char *ptr1 = _ptr1, *ptr2 = _ptr2;

      tmp = *ptr1;
      *ptr1 = *ptr2;
      *ptr2 = tmp;
    }
      break;
    case 2: {
      uint16_t tmp;
      uint16_t *ptr1 = _ptr1, *ptr2 = _ptr2;

      tmp = *ptr1;
      *ptr1 = *ptr2;
      *ptr2 = tmp;
    }
      break;
    case 4: {
      uint32_t tmp;
      uint32_t *ptr1 = _ptr1, *ptr2 = _ptr2;

      tmp = *ptr1;
      *ptr1 = *ptr2;
      *ptr2 = tmp;
    }
      break;
    case 8: {
      uint64_t tmp;
      uint64_t *ptr1 = _ptr1, *ptr2 = _ptr2;

      tmp = *ptr1;
      *ptr1 = *ptr2;
      *ptr2 = tmp;
    }
      break;
    default: {
      unsigned char buff[size];

      ft_memcpy (buff, _ptr1, size);
      ft_memcpy (_ptr1, _ptr2, size);
      ft_memcpy (_ptr2, buff, size);
    }
  }
}
