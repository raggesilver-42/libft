#include "garray-priv.h"

garray *
garray_new_full (size_t           element_size,
                 size_t           initial_size,
                 garray_grow_mode grow_mode,
                 t_free_func      *free_function)
{
  garray *self = ft_memalloc (sizeof *self);

  self->data_size = element_size;
  self->capacity = initial_size;
  self->data = ft_memalloc (element_size * (initial_size + 1));
  self->grow_mode = grow_mode;
  self->free_func = free_function;

  return self;
}

void
garray_destroy (garray **_self)
{
  if (!_self || !*_self) {
    return;
  }

  garray *self = *_self;

  if (self->free_func) {
    for (size_t i = 0; i < self->length; i++) {
      self->free_func (_GARRAY_POINTER_TO_INDEX (self, i));
    }
  }
  ft_strdel (&self->data);
  ft_memdel ((void **) _self);
}

void
garray_grow_full (garray *self,
                  size_t  minimum_capacity)
{
  size_t new_capacity = 0;

  switch (self->grow_mode) {
    case GARRAY_GROW_MODE_DOUBLE:
      new_capacity = MAX(minimum_capacity, self->capacity * 2);
      break;
  }

  size_t current_memory_size  = _GARRAY_GET_MEMORY_SIZE (self, self->capacity);
  size_t new_memory_size      = _GARRAY_GET_MEMORY_SIZE (self, new_capacity);

  self->data = ft_reallocsz (self->data, current_memory_size, new_memory_size);
  self->capacity = new_capacity;
}

void
garray_grow (garray *self)
{
  garray_grow_full (self, 0);
}

static inline void
garray_maybe_grow (garray *self)
{
  if (self->length + 1 > self->capacity) {
    garray_grow_full (self, 0);
  }
}

void *
garray_get_full (garray *self,
                 size_t  i)
{
  assert (i < self->length);
  return (void *) _GARRAY_POINTER_TO_INDEX (self, i);
}

void
garray_set_full (garray *self,
                 size_t i,
                 void   *data)
{
  // TODO: we currently assert that i is less than the array's length so that
  // it is impossible to set an element at a position greater than the length
  // but still within the `capacity` boundary.
  //
  // In a future iteration, we should allow that to happen. Setting an element
  // at position P where `length < P < capacity` will:
  //   - set length to P
  //   - fill in the gap between length and P with zeroes
  //
  // In an even further iteration, we can also allow setting items out of the
  // current boundary, in which case we simply resize the array as needed.
  assert (i < self->length);
  // Since we are overwriting an element, we must free it if the user has set
  // a free function
  if (self->free_func) {
    self->free_func (_GARRAY_POINTER_TO_INDEX (self, i));
  }
  ft_memcpy (_GARRAY_POINTER_TO_INDEX (self, i), data, self->data_size);
}

void
garray_push_full (garray  *self,
                  void    *data)
{
  garray_maybe_grow (self);

  ft_memcpy (_GARRAY_POINTER_TO_INDEX (self, self->length),
             data,
             self->data_size);
  self->length++;
}

void
garray_unshift_full (garray *self,
                     void   *data)
{
  garray_maybe_grow (self);

  if (self->length > 0) {
    ft_memmove (_GARRAY_POINTER_TO_INDEX (self, 1),
                self->data,
                _GARRAY_GET_MEMORY_SIZE (self, self->length));
  }

  ft_memcpy (_GARRAY_POINTER_TO_INDEX (self, 0), data, self->data_size);
  self->length++;
}

// TODO: go for a more useful approach (like JS' Array.slice/splice)

void
garray_remove_full (garray *self, size_t i, void *out_ptr)
{
  assert (i < self->length);

  if (out_ptr) {
    ft_memcpy (out_ptr, _GARRAY_POINTER_TO_INDEX (self, i), self->data_size);
  }

  // If we removed the last element fill it with zeroes
  if (i + 1 == self->length) {
    ft_memset (_GARRAY_POINTER_TO_INDEX (self, i), 0, self->data_size);
  }
  // Otherwise, shift the whole array back 1 slot starting from the index we
  // just removed an item from
  else {
    ft_memmove (_GARRAY_POINTER_TO_INDEX (self, i),
                _GARRAY_POINTER_TO_INDEX (self, i + 1),
                _GARRAY_GET_MEMORY_SIZE (self, (self->length - i - 1)));
  }

  self->length--;
}

void
garray_sort (garray *self, t_garray_compare_func *cmp)
{
  // TODO: Decide which algorithm is best
  //  garray_dumb_sort(self, cmp);
  //  garray_quick_sort (self, cmp);

  garray_quick_sort (self, cmp);
}
