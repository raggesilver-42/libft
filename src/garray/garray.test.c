
#include "libft.h"
#include <assert.h>

int test()
{
  ft_printf ("I was called");
  return 0;
}

int
compare_int_ptrs (void *_ptr1, void *_ptr2)
{
  return (*((int *)_ptr1) - *((int *)_ptr2));
}

int
compare_chararray_ptrs (void *_ptr1, void *_ptr2)
{
  return ft_strcmp (*(char **)_ptr1, *(char **)_ptr2);
}

int main(void)
{
  // Basic usage with literals
  {
    autofree(garray) *arr = garray_new (int);
    garray_push (arr, 2);
    assert (garray_get (arr, int, 0) == 2);
  }
  // Basic auto-resize test
  {
    autofree(garray) *arr = garray_new (int);

    for (int i = 0; i < 200; i++) {
      garray_push (arr, i);
    }

    assert (arr->length == 200);
  }
  // Basic usage with pointers
  {
    autofree(garray) *arr = garray_new (char *);
    arr->free_func = (t_free_func *) &ft_strdel;

    garray_push (arr, ft_strdup ("item 1"));
    garray_push (arr, ft_strdup ("item 2"));

    assert (ft_strequ (garray_get (arr, char *, 0), "item 1"));
    assert (ft_strequ (garray_get (arr, char *, 1), "item 2"));
  }
  // Sorting literals
  {
    autofree(garray) *arr = garray_new (int);

    garray_push (arr, 9);
    garray_push (arr, 0);
    garray_push (arr, 2);
    garray_push (arr, 24);
    garray_push (arr, 3);

    garray_sort (arr, (t_garray_compare_func *)&compare_int_ptrs);

    for (size_t i = 1; i < arr->length; i++) {
      int a = garray_get (arr, int, i - 1);
      int b = garray_get (arr, int, i);
      ft_printf("a: %d, b %d\n", a, b);
      assert (a <= b);
    }

    ft_printf ("Sorted array:");
    for (size_t i = 0; i < arr->length; i++) {
      ft_printf (" %d", garray_get (arr, int, i));
    }
    ft_printf ("\n");
  }
  // Sorting pointers
  {
    autofree(garray) *arr = garray_new (char *);
    arr->free_func = (t_free_func *) &ft_strdel;

    garray_push (arr, ft_strdup("vincent"));
    garray_push (arr, ft_strdup("abed"));
    garray_push (arr, ft_strdup("troy"));
    garray_push (arr, ft_strdup("annie"));
    garray_push (arr, ft_strdup("carlos"));

    garray_sort (arr, (t_garray_compare_func *)&compare_chararray_ptrs);

    for (size_t i = 1; i < arr->length; i++) {
      char *a = garray_get (arr, char *, i - 1);
      char *b = garray_get (arr, char *, i);
      assert (ft_strcmp (a, b) <= 0);
    }

    ft_printf ("Sorted array:");
    for (size_t i = 0; i < arr->length; i++) {
      ft_printf (" '%s'", garray_get (arr, char *, i));
    }
    ft_printf ("\n");
  }
  // Overwriting pointers
  {
    autofree(garray) *arr = garray_new (char *);
    arr->free_func = (t_free_func *) &ft_strdel;

    garray_push (arr, ft_strdup ("paulo"));
    garray_set (arr, 0, ft_strdup ("queiroz"));

    assert (ft_strequ (garray_get (arr, char *, 0), "queiroz"));
  }
  // Foreach
  {
    autofree(garray) *arr = garray_new (int);

    garray_push (arr, 0);
    garray_push (arr, 1);
    garray_push (arr, 2);
    garray_push (arr, 3);

    garray_foreach (arr, int, i, item, {
      assert (item == (int) i);
    });

    assert (garray_get (arr, int, 0) == 0);
  }
  return 0;
}
