
#ifndef GARRAY
# define GARRAY

# ifndef GARRAY_INITIAL_SIZE
#  define GARRAY_INITIAL_SIZE 10
# endif

# include "libft.h"
# include "algorithms/algorithms.h"
# include <assert.h>

typedef void (t_free_func)(void *);

typedef int (t_garray_compare_func)(void *, void *);

enum e_garray_grow_mode {
  GARRAY_GROW_MODE_DOUBLE,
};

typedef enum e_garray_grow_mode garray_grow_mode;

struct        s_garray {
  /** Number of elements in this array */
  size_t            length;
  /** Size (in bytes) of each element in this array */
  size_t            data_size;
  /** Amount of items that can be stored before having to resize the array */
  size_t            capacity;
  garray_grow_mode  grow_mode;
  /**
   * A function to free each array element when freeing the array itself. Do not
   * set a free function if your array is storing non heap-allocated values.
   *
   * NOTE: a pointer to each element will be passed to this function, that is,
   * for an array of integers, this function will receive (int *). For a string,
   * this function will receive (char **).
   *
   * @nullable
   */
  t_free_func       *free_func;
  char              *data;
};

typedef struct s_garray garray;

garray  *garray_new_full (size_t            element_size,
                          size_t            initial_size,
                          garray_grow_mode  grow_mode,
                          t_free_func       *free_function);
# define garray_new(type) garray_new_full (sizeof (type), \
                                           GARRAY_INITIAL_SIZE, \
                                           GARRAY_GROW_MODE_DOUBLE, \
                                           NULL)

void     garray_destroy (garray **self);

void    *garray_get_full (garray *self, size_t i);

# define garray_get_fast(arr, type, i) ((type *)(arr->data))[i]

# define garray_get(arr, type, i) ({ \
    assert (sizeof (type) == arr->data_size); \
    assert (i < arr->length);        \
    garray_get_fast(arr, type, i); \
  })

void      garray_set_full (garray *self, size_t i, void *data);
# define  garray_set(arr, i, value) ({ \
    typeof ((value)) _tmp = (value); \
    assert (sizeof ((value)) == arr->data_size); \
    garray_set_full (arr, i, &_tmp); \
  })

void      garray_push_full (garray *self, void *data);

/**
 * @brief Add an element to the end of an array
 */
# define  garray_push(arr, value) ({ \
    typeof ((value)) _tmp = (value); \
    assert (sizeof (_tmp) == arr->data_size); \
    garray_push_full (arr, &_tmp); \
  })

void      garray_unshift_full (garray *self, void *data);
# define  garray_unshift(arr, value) ({ \
    typeof ((value)) _tmp = (value); \
    assert (sizeof (_tmp) == arr->data_size); \
    garray_unshift_full (arr, &_tmp); \
  })

void      garray_remove_full (garray *self, size_t i, void *out_ptr);
# define  garray_take(arr, type, i) ({ \
    type _tmp; \
    garray_remove_full (arr, i, &_tmp); \
    _tmp; \
  })
# define  garray_remove(arr, i) garray_remove_full(arr, i, NULL)
# define  garray_pop(arr, type) ({ \
    assert (arr->length > 0);      \
    type _tmp;                     \
    garray_remove_full (arr, arr->length - 1, &_tmp); \
    _tmp;                          \
  })

/**
 * Sort array in place.
 *
 * @param self
 * @param cmp
 */
void    garray_sort (garray *self, t_garray_compare_func *cmp);

#define garray_foreach(arr, type, counter, item_name, block) ({    \
    type item_name;                                                \
    for (size_t counter = 0; counter < arr->length; counter++) {   \
      item_name = garray_get (arr, type, counter);                 \
      block;                                                       \
    }                                                              \
  })

#endif
