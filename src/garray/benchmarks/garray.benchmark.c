
#include "libft.h"

__attribute__((flatten))
int
compare_size_t_ptrs (void *_ptr1, void *_ptr2)
{
  return (*((size_t *)_ptr1) - *((size_t *)_ptr2));
}

int main (void)
{
  // autofree(garray) *arr = garray_new (size_t);
  autofree(garray) *arr = garray_new_full (sizeof (size_t),
                                           1000000,
                                           GARRAY_GROW_MODE_DOUBLE,
                                           NULL);

  for (size_t i = 0; i < 1000000; i++) {
    garray_push (arr, i);
  }

  garray_sort (arr, (t_garray_compare_func *) &compare_size_t_ptrs);

  return 0;
}
