#ifndef ALGORITHMS_PRIV_H
# define ALGORITHMS_PRIV_H

# include "libft.h"

/**
 * Quick sort implementation for garray. This is **in-place** sorting.
 *
 * @param self garray to be sorted
 * @param cmp a function used to compare items in the array
 */
void
garray_quick_sort (garray                *self,
                   t_garray_compare_func *cmp);

void
garray_dumb_sort (garray                 *self,
                  t_garray_compare_func  *cmp);


#endif
