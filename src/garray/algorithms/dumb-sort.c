
#include "../garray-priv.h"

void
garray_dumb_sort (garray                *self,
                  t_garray_compare_func *cmp)
{
  for (size_t i = 0; i < self->length; i++) {
    for (size_t j = i + 1; j < self->length; j++) {
      void *a = _GARRAY_POINTER_TO_INDEX(self, j);
      void *b = _GARRAY_POINTER_TO_INDEX(self, i);
      if (cmp (a, b) < 0) {
        ft_memswap(a, b, self->data_size);
      }
    }
  }
}
