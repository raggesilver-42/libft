#ifndef ALGORITHMS_H
# define ALGORITHMS_H

enum e_sort_algorithm {
  QUICK_SORT,
};

typedef enum e_sort_algorithm e_sort_algorithm;

#endif
