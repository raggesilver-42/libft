
#include "../garray-priv.h"

static size_t
_garray_quick_sort_partition (garray *self,
                              size_t low,
                              size_t high,
                              t_garray_compare_func *cmp)
{
  size_t pivot_index = (low + high) / 2;

  if (pivot_index != high) {
    ft_memswap (_GARRAY_POINTER_TO_INDEX(self, high),
                _GARRAY_POINTER_TO_INDEX(self, pivot_index),
                self->data_size);
  }

  size_t i = low;
  void *pivot = _GARRAY_POINTER_TO_INDEX(self, high);

  for (size_t j = low; j < high; j++) {
    if (cmp (_GARRAY_POINTER_TO_INDEX(self, j), pivot) < 0) {
      ft_memswap (_GARRAY_POINTER_TO_INDEX(self, j),
                  _GARRAY_POINTER_TO_INDEX(self, i),
                  self->data_size);
      ++i;
    }
  }

  ft_memswap (_GARRAY_POINTER_TO_INDEX(self, i),
              _GARRAY_POINTER_TO_INDEX(self, high),
              self->data_size);

  return i;
}

static void
_garray_quick_sort_recursion (garray *self,
                              size_t low,
                              size_t high,
                              t_garray_compare_func *cmp)
{
  if (low >= high) {
    return;
  }

  size_t pivot = _garray_quick_sort_partition (self, low, high, cmp);

  if (pivot > 0) {
    _garray_quick_sort_recursion (self, low, pivot - 1, cmp);
  }
  _garray_quick_sort_recursion (self, pivot + 1, high, cmp);
}

void
garray_quick_sort (garray *self,
                   t_garray_compare_func *cmp)
{
  _garray_quick_sort_recursion (self, 0, self->length - 1, cmp);
}
