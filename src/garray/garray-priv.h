#ifndef GARRAY_PRIV_H
# define GARRAY_PRIV_H

# include "libft.h"
# include "algorithms/algorithms-priv.h"

# define _GARRAY_INDEX(arr, i) ((arr)->data_size * (i))
# define _GARRAY_POINTER_TO_INDEX(arr, i) (&(arr)->data[_GARRAY_INDEX ((arr), (i))])
# define _GARRAY_GET_MEMORY_SIZE(arr, n) ((n) * (arr)->data_size)

#endif
