#ifndef FT_TYPES_H
# define FT_TYPES_H

# define autofree(type) __attribute__((cleanup(type ## _destroy))) type

#endif
